---
title: "Curriculum Vitae"
date: 2022-03-01T21:35:38+05:30
author: "Athul R T"
page : true
draft: false
subtitle: ""
description: ""
license: ""
images: []
type : "page"
tags: [cv, about]
categories: [about]
featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
fontawesome: true
license: ''

toc:
  enable: true
  auto: true
code:
  copy: true
  # ...
table:
  sort: true
  # ...
math:
  enable: true
  # ...
share:
  enable: true
---

# Athul R T



## Work Experience

-   **Project Manager** at R&D Division, Msigma Gokulam Private Ltd., Trivandrum, India
    (March 2022 - Present)

-   **Researcher & Developer** at Zackriya Solutions, Bengaluru, India
    (November 2020 - October 2021)

-   **Technical Assistant** at Department of Physics, Cochin University
    of Science and Technology (CUSAT), Kochi, India (September 2020 --
    February 2021)

-   **Technical Assistant** at the Inter University Centre for studies
    on Kerala Legacy of Astronomy and Mathematics (IUCKLAM), CUSAT,
    Kochi, India (November 2019 -- March 2020)

-   **Project Assistant** under **Dr. Rhine Kumar A. K.** (Asistant
    Professor, CUSAT), DST -- INSPIRE project on studied theoretical
    aspects of neutron star equation of state using Relativistic Mean
    Field theory. (March -- November 2019)


## Research Experience

-   **Higher Order Couplings in Neutron Stars**.\
    Supervisor: **Dr. Rhine Kumar A. K.**, Assistant Professor, CUSAT
    (March -- November 2019).\
    Studied theoretical aspects of Neutron Star equation of state using
    Relativistic Mean Field theory.

-   **Spectral and Timing Analysis of Swift J1658.2-4242 using Astrosat
    data**.\
    Supervisor: **Prof. Ranjeev Misra**, IUCAA, Pune (July 2018).\
    Visiting student at IUCAA, Pune on X-ray astronomy data analysis of
    Swift J1658.2-4242.

-   **Simulation Studies of Projected Central Light Distribution of
    Galaxies**.\
    Supervisor: **Dr. C. D. Ravikumar**, Associate Professor, University
    of Calicut (April -- June 2018).\
    Simulated a lenticular galaxy and studied the variation of Central
    Intensity Ratio (CIR) with its orientation.

## Publications

-   V. Jithesh, Bari Maqbool, Ranjeev Misra, **Athul R. T.**, Gitika
    Mall, Marykutty James. **Spectral and Timing Properties of the
    Galactic X--ray transient Swift J1658.2--4242 using Astrosat
    Observations**, [The Astrophysical
    Journal](https://doi.org/10.3847/1538-4357/ab4f6a) **887**, 1
    (December 2019).\
    arXiv: [1910.07804](https://arxiv.org/abs/1910.07804)
    \[astro-ph.HE\] (**2019**).


-   **M.Phil. Physics**, Department of Physics, CUSAT, Kochi, India.
    (November, 2018), 75%

    -   **Studies on Orientation Effects of Disk Galaxies: A Monte Carlo
        Approach**, 2018, Supervisors: **Prof. Titus K. Mathew (CUSAT),
        Dr. C. D. Ravikumar (University of Calicut),** Study of
        variation in the radial profile of spiral and lenticular
        galaxies by simulating them using Monte Carlo method.

-   **M.Sc. Physics**, Department of Physics, University of Kerala,
    Thiruvananthapuram, India. (July, 2015), 86%.

    -   **Standard Lambda -- CDM Model: A Review**, 2015, supervisor:
        **Prof. Titus K. Mathew (CUSAT)**, A review of currently
        accepted model of universe, cold dark matter with cosmological
        constant.

-   **B.Sc. Physics**, University of Kerala, India. (May, 2013), 91.5%.

    -   **A Qualitative Study on Dark Matter**, 2013, supervisor:
        **Dr. V. Sanalkumaran Nair (VTMNSS College)**, Analysis of
        different methods used to advocate the presence of dark matter
        in the universe .

## Conference Presentations

-   Regional Astronomers Meet V -- Astronomy Research: Opportunities and
    Challenges organized by IUCAA at CUSAT, Kochi, 2019. on the title
    **Studies on Orientation Effects of Disk Galaxies using Monte Carlo
    Method.**

## Education

-   **M.Phil. Physics**, Department of Physics, CUSAT, Kochi, India.
    (November, 2018), 75%

    -   **Studies on Orientation Effects of Disk Galaxies: A Monte Carlo
        Approach**, 2018, Supervisors: **Prof. Titus K. Mathew (CUSAT),
        Dr. C. D. Ravikumar (University of Calicut),** Study of
        variation in the radial profile of spiral and lenticular
        galaxies by simulating them using Monte Carlo method.

-   **M.Sc. Physics**, Department of Physics, University of Kerala,
    Thiruvananthapuram, India. (July, 2015), 86%.

    -   **Standard Lambda -- CDM Model: A Review**, 2015, supervisor:
        **Prof. Titus K. Mathew (CUSAT)**, A review of currently
        accepted model of universe, cold dark matter with cosmological
        constant.

-   **B.Sc. Physics**, University of Kerala, India. (May, 2013), 91.5%.

    -   **A Qualitative Study on Dark Matter**, 2013, supervisor:
        **Dr. V. Sanalkumaran Nair (VTMNSS College)**, Analysis of
        different methods used to advocate the presence of dark matter
        in the universe .


## Awards & Scholarships

-   **Aspire Scholarship** by Government of Kerala (2018)[^1]

-   **Higher Education Scholarship** by Government of Kerala for B.Sc.
    and M.Sc. (2010 -- 2015)[^2]

-   **British Council Aptis Test** with score 158/200 (2013)[^3]


## Technical Skills

-   OS : GNU/Linux (Ubuntu, Debian, Arch Linux, Fedora), Windows.

-   Programming Languages : Python (Numpy, Matplotlib, Scipy, Astropy,
    Scikit-learn, Keras, Statmodels etc.), Julia, C, C++.

-   Scientific Softwares : Gnuplot, HEASOFT, Mathematica, Origin, IRAF,
    DS9.

-   Others: LaTeX, git, Bash Shell Scripting, Server Management,
    Parallel Processing etc.

## Online Courses and Certifications

-   **Introduction to Genomic Technologies**, an online non-credit
    course authorized by Johns Hopkins University offered through
    Coursera, July 28, 2020.

-   **Julia Scientific Programming** with honors, an online non-credit
    course offered by University of Cape Town offered through Coursera,
    July 24, 2020.

-   **COVID--19 Contact Tracing**, an online non-credit course
    authorized by Johns Hopkins University offered through Coursera,
    June 11, 2020.

-   **COVID--19 Data Analysis Using Python**, an online non-credit
    course offered by Coursera Project Network, June 11, 2020.

-   **Data-driven Astronomy**, an online non-credit course authorized by
    The University of Sydney offered through Coursera, June 7, 2020.

## Conferences and Workshops

-   Two day International Collaboration and Co-working Event focused on
    NASA's TESS Mission. September 8--10, 2020.

-   National Seminar on X--Ray Astronomy, organized by IUCAA Pune and
    Dept. of Physics, St. Thomas College, Ranni, Kerala, February
    1--2, 2020.

-   Workshop on Emergent Gravity Paradigm, organized by IUCAA Pune and
    ICARD, Dept. of Physics, CUSAT, Kochi, November 8 -- 10, 2019.

-   Regional Astronomers Meet V -- Astronomy Research: Opportunities and
    Challenges by IUCAA, Pune and Dept. of Physics, CUSAT, February 8 --
    9, 2019.

-   Advances in Observational Astronomy, Department of Physics,
    University of Calicut, December 12 -- 14, 2018.

-   Mini School on X-ray Astronomy, by APT Kerala and IUCAA Pune,
    Providence Women's College, May 22-26, 2018.

-   UGC Sponsored National Workshop on Astronomy and Astrophysics,
    Farook College, Calicut, February 14 -- 15, 2018.

-   National Workshop on Optical and UV Astronomical Data Analysis,
    Sacret Heart College, Chalakkudy, January 6 -- 7, 2018.

## Invited Talks

-   **Trainer on TeX installation and Basics** in two day workshop, L
    for LaTeX by Poetry of Reality, February 5, 2022.

-   **Trainer on Astronomical Image Processing** in two day workshop on
    Radio Astronomy organized by Positron Foundation, Govt. Engineering
    College, Kannur, Kerala, March 1, 2020.

-   **Trainer on Astronomical Image Processing** in two day workshop on
    Astrophotography organized by Positron Foundation, St. Teresa's
    College, Kochi, Kerala, February 21, 2020.

-   **Talk on Solar Eclipse** at Morning Star Home Science College,
    Angamaly, December 20, 2019.

-   **Talk on Eclipse and Career in Astronomy** at SN College Kannur,
    December 13, 2019.

-   **Trainer on Python for Scientific Computing and LaTeX**, A Short
    term Course on Gravitation and Cosmology for College Teachers
    organized by APT Kerala and IUCAA Pune at Providence College,
    Kozhikode, November 27, 2019.

-   **Talk on Demonstration of Computer based tools for Solar Eclipse**
    at CUSAT (November 16, 2019) and at Govt. College, Madapally,
    Kozhikode (November 23, 2019) organized by IUCKLAM, CUSAT.

-   **Talk on Solar Eclipse**, UNICEF funded tribal students training
    programme at Thodupuzha, November 3, 2019.

-   **Invited talk on History and Physics of Space missions** at Aquinas
    College, Edakochi, August 1, 2019.

-   **Talk on Apollo Missions** at CUSAT, Workshop on Scientific
    Awareness, by ICARD, IUCKLAM and KSSP, July 12, 2019.

## Leadership and Outreach

-   **Volunteer** at Covid--19 relief activities under District Disaster
    Management Authority(DDMA) Thiruvananthapuram from May 2020

-   **Member** at Amateur Astronomers Association, Kerala (AASTRO
    Kerala) from 2012.

-   **Member** at Free Software Community of India from 2018.

-   **Editor** at Malayalam Wikipedia, Wikimedia Commons, Wikidata.

-   **Volunteer** at Zooniverse Projects (Gravity Spy, Galaxy Zoo,
    Planet Hunters).

-   **Telescope Demonstrator** at Open House, Department of Physics,
    CUSAT (2018, 2019).

-   **Facilitator** at Physics: Scope and Awareness workshop for school
    students (2018, 2019).


[^1]: Government of Kerala Aspire Scholarship provides opportunities to
    pursue short term internships/projects.

[^2]: Government of Kerala provides financial support for the
    meritorious students till M.Sc.

[^3]: Aptis provides reliable, accurate results about the English
    skills.
