---
title: "Other Projects"
date: 2022-03-01T23:49:30+05:30
author: "Athul R T"
draft: false
subtitle: ""
description: ""
license: ""
images: []
tags: [projects, about]
categories: [about]
featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: true
hiddenFromSearch: false
twemoji: false
lightgallery: true
fontawesome: true
license: ''

toc:
  enable: true
  auto: true
code:
  copy: true
  # ...
table:
  sort: true
  # ...
math:
  enable: true
  # ...
share:
  enable: true
---
### Open Source Contributions


-   Webapp for extracting Text from PDF \
    [Webapp Link](https://convertpdf2txt.herokuapp.com/)

    -   Webapp created for extracting text from PDF pages using pymupdf
        as background engine.

-   Webapp for Forex Prediction
    [Webapp Link](predict-currency.herokuapp.com/)

    -   Webapp created using plotly dash framework in Python for
        prediction of various currency rates using Prophet machine
        learning algorithm.

-   Brilliance Scrap -- A voluntary project for PSC aspirants in
    Telegram (April 2020) \[[Gitlab
    Link](https://gitlab.com/athulvis/brilliance-scrap)\]

    -   Developed a webscrapping tool for parsing previous Kerala PSC
        question papers and post them to a Telegram channel.

    -   Used Python modules such as Beautiful Soup for parsing and
        sqlite3 for database creation.

-   Talent Scrap -- A project for PSC aspirants (April 2020) \[[Gitlab
    Link](https://gitlab.com/athulvis/talent-scrap)\]

    -   A web scraping project to parse daily current affairs posts from
        Talent Academy website and create a pdf file of it.

    -   Used Python modules Beautiful Soup for parsing and img2pdf for
        image to pdf conversion.

-   FORTRAN Input Update -- bash script for FORTRAN code input (October
    2019). \[[Gitlab
    Link](https://gitlab.com/athulvis/fortran-input-update)\]

    -   A Bash script to update values in a FORTRAN program file for
        Nuclear Physics studies.

## Volunteer Contributions

-   Forecasting of Covid-19 cases using Machine Learning Algorithms (2021 -- 2022)

    -   Forecasting Covid-19 cases in Kerala state using Machine
        learning algorithms and created a dashboard for presenting the
        results with visualization for Kerala State Disaster Management.

- Website for District Medical Office, Thiruvananthapuram

    [Website Link](https://www.dmohtrivandrum.in)

    - A website based on Wordpress was build for sharing Covid and health regarded information for District Medical Office, Thiruvananthapuram
