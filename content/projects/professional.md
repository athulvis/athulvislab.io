---
title: "Professional Projects"
date: 2022-03-01T23:46:48+05:30
author: "Athul R T"
draft: false
subtitle: ""
description: ""
license: ""
images: []
tags: [projects, about]
categories: [about]
featuredImage: ""
featuredImagePreview: ""
hiddenFromHomePage: true
hiddenFromSearch: false
twemoji: false
lightgallery: true
fontawesome: true
license: ''

toc:
  enable: true
  auto: true
code:
  copy: true
  # ...
table:
  sort: true
  # ...
math:
  enable: true
  # ...
share:
  enable: true
---

## Zackriya Solutions


-   Crytocurrency Exchange Comparison Webapp (September 2021 --
    October 2021)

    -   Created webapp for comparison of Wazrix and Binance exchange
        rates for different cryptocurrencies using Flask and Pandas.

-   Network Visualization Dashboard of Cryptocurrency transactions
    (September 2021 -- October 2021)

    -   Created dashboard for network visualization of block chain data
        of cryptocurrency transactions. Used Python packages networkx
        and pandas for network analysis and Plotly, Dash for creating
        visualization and dashboard.

-   Python package for creating custom pytest report in JSON format.
    (August 2021 -- September 2021)

    -   Created python package to generate custom JSON report from
        pytest output. [Pypi Link](https://pypi.org/project/pytest-railflow-testrail-reporter/)

-   Time series analysis of economic data using Machine Learning (July
    2021 -- present)

    -   Analyzed time series data from api source using various machine
        learning algorithms.

-   Design and Implementation of statistical functions in API backend
    (June 2021)

    -   Designed and implemented statistical algorithm in the api server
        backend using Python for processing time series data.

-   GUI automated Test Interface - GaTI (March 2021 -- May 2021)

    -   Python based tool for automated GUI testing with features like
        error reporting, log generation, continuous integration,
        continuous deployment along with custom APIs for developers.

-   Data Wrangling and Analysis (January 2021 -- June 2021)

    -   Created Python based tool for wrangling and analysis of big
        datasets from real estate.

-   Object Detection for AI based video game backend (February 2021)

    -   Training and testing of object detection models for AI based
        backend for a video game.

-   Automated Testing for a medical application GUI (November 2020 --
    January 2021)

    -   Wrote codes for automating tests of an application GUI using
        Pytest package.


## Freelance Projects

-   Technical Support for Positron Foundation (August 2020 -- present)

    -   Created a bash script to install DL\_POLY, a molecular
        simulation software for an online course.

-   Technical Support for Travancore Academy -- IT Support for Online
    Education Firm based on Telegram (June 2020)

    -   Developed a quizbot for automatic posting for quiz on Telegram.

    -   Developed a customized image editor for automating image
        processing using Pillow module of Python.

-   Technical Support for Alpha 2 Omega, Kochi based CSIR NET/GATE
    Coaching Centre (November 2019)

    -   Developed a program for evaluation of the results of online
        examination. Used Python with Numpy and Pandas module.
