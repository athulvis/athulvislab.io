---
title: "Research Projects"
date: 2022-03-01T23:34:48+05:30
date: 2022-03-01T21:35:38+05:30
author: "Athul R T"
draft: false
subtitle: ""
description: ""
license: ""
images: []
type : "page"
tags: [projects, about]
categories: [about]
featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: true
hiddenFromSearch: false
twemoji: false
lightgallery: true
fontawesome: true
license: ''

toc:
  enable: true
  auto: true
code:
  copy: true
  # ...
table:
  sort: true
  # ...
math:
  enable: true
  # ...
share:
  enable: true
---
## Astronomy Projects

-   Collaborator at Project
    JazzHands\
    [Github Link](https://github.com/project-wavelets/JazzHands)\
    [Pypi Link](https://pypi.org/project/jazz-hands/)

    *   Contributed in testing and documentation of JazzHands, a Python
        package for analysis of unevenly sampled timeseries from NASA
        TESS data.


-   Galaxy Simulation codes -- MPhil Project (October 2018) \
    [Gitlab Link](https://gitlab.com/athulvis/galaxy-simulation-codes)

    *   Created simulated spiral and lenticular galaxies using Monte
        Carlo method. Used Python modules Numpy, Scipy and Matplotlib
        for simulation, computation and plotting of galaxies.
