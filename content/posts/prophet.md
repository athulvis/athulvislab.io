---
title: "Prophet Hyper parameter Tuning"
subtitle: "Hyperparameters and their descriptions from Prophet website"
date: 2021-06-11T00:27:26+05:30
lastmod: 2021-06-25T00:27:26+05:30
author: "Athul R T"
description: "Hyperparameters and their descriptions from Prophet website"

tags: [python, ML]
categories: [article]
series: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: true
lightgallery: true
draft: true
---

Options inside prophet model class.
## 1. `growth`

Maximum or minimum achievable points.

Options
------------
-  `linear` - No limit for achievable point
-  `logistic - There is maximum or minimum limit. May be `cap` or `floor`. Cap is the upper limit and floor is the lower limit

Default is linear

Usage
----------
+ Logistic
```
df['cap'] = 8.5
df['floor'] = 1.5
m = Prophet(growth='logistic')
m.fit(df)
```
+ Linear
```
m = Prophet(growth='linear')
m.fit(df)
```
## 2. Trend Change Points

Prophet assumes 25 changepoints uniformly distributed in 80% of data points.
### 1. n_changepoints

No. of potential change points. We can customize number of changepoints in data. (range 0 to n)

### 2.  changepoint_range

Range of time series in which Prophet considers change points. By default it is 0.8 (80%). It can be customized from 0 to 1.

### 3. changepoint_prior_scale

Can be adjusted to avoid overfit (highly flexible) or underfit (low flexibility) due to trend changes. By default it is 0.005. It can be modified accordingly. (0 to n)

### 4. changepoints

Specify points at which change occurs. List of dates can be given as input.

eg: `changepoints=['2014-01-01', '2018-01-01']`

##### To visualize changepoints,

```
# Python
from prophet.plot import add_changepoints_to_plot
fig = m.plot(forecast)
a = add_changepoints_to_plot(fig.gca(), m, forecast)
```
## 3. Seasonality

### holidays

Holidays or reccurring events can be modelled in prophet. We have to create a daframe for that with two columns, `holiday` and `ds`. It should include past and future occurences of holidays.

`lower_window` and `upper_window` can also be included in holiday dataframe. It mentions how much it is extended around the holidays. ie, for including Chistmas eve along with Christmas holiday, use `lower_window=-1,upper_window=0` (for previous day). for next day effect, use `lower_window=0,upper_window=1`

`prior_scale` can be set for each holiday too.

sample:

```
playoffs = pd.DataFrame({
  'holiday': 'playoff',
  'ds': pd.to_datetime(['2008-01-13', '2009-01-03', '2010-01-16',
                        '2010-01-24', '2010-02-07', '2011-01-08',
                        '2013-01-12', '2014-01-12', '2014-01-19',
                        '2014-02-02', '2015-01-11', '2016-01-17',
                        '2016-01-24', '2016-02-07']),
  'lower_window': 0,
  'upper_window': 1,
})
superbowls = pd.DataFrame({
  'holiday': 'superbowl',
  'ds': pd.to_datetime(['2010-02-07', '2014-02-02', '2016-02-07']),
  'lower_window': 0,
  'upper_window': 1,
})
holidays = pd.concat((playoffs, superbowls))
m = Prophet(holidays=holidays)
forecast = m.fit(df).predict(future)
fig = m.plot_components(forecast) #plots companents
```
### Built-in Country Holidays

Country holidays can be specified

```
m = Prophet(holidays=holidays)
m.add_country_holidays(country_name='US')
m.fit(df)
m.train_holiday_names
```

### References

- [Prophet website - Documentation](https://facebook.github.io/prophet/docs/installation.html#python)
