---
title: "Add your Python Package to conda-forge"
subtitle: "Tutorial to add your package to conda-forge."
date: 2021-07-21T00:24:42+05:30
lastmod: 2021-07-21T00:24:42+05:30
author: "Athul R T"
description: "Tutorial to add your package to conda-forge."

tags: [python]
categories: [article]
series: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: true
lightgallery: true
draft: false
---
<!--more-->

1. Fork conda-recipes repostiory to your github account.

2. Clone the repo to your local system. Checkout to a new branch.

3. Open recipes folder and open terminal there.

4. Install `grayskull`

    ```pip3 install grayskull```

5. Now run

    ```grayskull pypi  <package name> ```

    The package should be in pypi package index.

6. Now you can see a new folder with your package name and `meta.yml` file inside the folder. Now tweak the `meta.yml` file inside the package folder. You can refer `meta.yml` file inside `example` folder for reference.

7. Install conda-smithy if it isn't installed in your system

    ```conda install -n root -c conda-forge conda-smithy```

8. Now open the newly created folder with your package name. Run

    ```conda smithy recipe-lint .```

    The smithy will show lints, or errors in the meta.yml file. Fix them and update your repository.

9. Now create a PR to conda-recipes repo master branch with title `Adding <package name> recipe`. Wait till the maintainers review and approve it.
