---
title: "Gnuplot Installation in Gnu/Linux"
subtitle: "A tutorial for installation and basic usage of Gnuplot, a plotting tool."
date: 2020-08-29T00:16:30+05:30
lastmod: 2020-08-29T00:16:30+05:30
author: "Athul R T"
description: "A tutorial for installation and basic usage of Gnuplot, a plotting tool."

tags: [gnuplot, linux]
categories: [article]
series: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: true
lightgallery: true
draft: false
---
<!--more-->


Gnuplot is a CLI (Command - Line Interface) based plotting software which can be used in GNU/Linux based systems (Ubuntu, Debian, Fedora etc), Windows, Mac OS etc which was first released in 1986.  It is mainly used by scientists and engineers for visualizing and analyzing the data. It is open source, free to download and modifications can be made according to the Gnuplot License. As it is CLI based, it doesn't have a graphical interface and is operated by giving instructions through terminal.

### Advantages
1. It can read data from multiple softwares despite of their formats.

3. It can produce 2D, 3D, parametric, multiplots, contour plots etc in different styles and coordinate systems.
4. It is fast and uses less resources than other plotting softwares.

## Installing Gnuplot

Gnuplot can be installed in Ubuntu/Debian based operating systems by the following method.

![install gnuplot](/gnuplot/1.png)

Open Terminal. Run the below command

```sudo apt install gnuplot```

Enter your password (it won't appear in the screen) and press enter.

 It may ask if you want to continue. Then enter **y**. That's all. Gnuplot is installed.

![install gnuplot](/gnuplot/2.png)

Now run **gnuplot** by entering `gnuplot` to the terminal and press enter.

It will appear as in the gif below.

![run gnuplot](/gnuplot/3.gif)

You can create a test data file with some `x` and `y` values.
