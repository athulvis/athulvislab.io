---
title: "Hosting in heroku: Issues and Solutions"
subtitle: "Common Problems and Solutions during Heroku hosting"
date: 2021-06-11T00:34:02+05:30
lastmod: 2022-06-11T00:34:02+05:30
author: "Athul R T"
description: "Common Problems and Solutions during Heroku hosting"

tags: [heroku, hosting]
categories: [article]
series: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: true
lightgallery: true
draft: false
---
<!--more-->


### 1. Failed to push some refs to git

   ```Your account has reached its concurrent builds limit
   To https://git.heroku.com/covid19-forecast-kerala.git
 ! [remote rejected] main -> main (pre-receive hook declined)
error: failed to push some refs to 'https://git.heroku.com/covid19-forecast-kerala.git' ```


#### Solution
  Run `heroku builds:cancel`. Then run `git push heroku main`

### 2.  Failed to find attribute 'server' in 'app'

  Check gunicorn run from local machine: `gunicorn app:server --preload -b 0.0.0.0:5000 ` Run this command from the same folder.

  Add `server = app.server` under `app = dash.Dash()` in `app.py`
