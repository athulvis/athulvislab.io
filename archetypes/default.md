---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
date: {{ .Date }}
lastmod: {{ .Date }}
author: "Athul R T"
description: ""

tags: []
categories: []
series: []

hiddenFromHomePage: false
hiddenFromSearch: false

featuredImage: ""
featuredImagePreview: ""

toc:
  enable: true
math:
  enable: true
lightgallery: true
draft: true
---
<!--more-->
